package tech.mastertech.itau.cartao.cartao.controllers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import tech.mastertech.itau.cartao.cartao.models.Bandeira;
import tech.mastertech.itau.cartao.cartao.models.Cartao;
import tech.mastertech.itau.cartao.cartao.models.Cliente;
import tech.mastertech.itau.cartao.cartao.models.Lancamento;
import tech.mastertech.itau.cartao.cartao.repositories.LancamentoRepository;
import tech.mastertech.itau.cartao.cartao.services.LancamentoService;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = LancamentoController.class)
public class LancamentoControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private LancamentoService lancamentoService;
	private Lancamento lancamento;
	private ObjectMapper mapper = new ObjectMapper();

	@MockBean
	private LancamentoRepository lancamentoRepository;

	@Before
	public void preparar() {
		lancamento = new Lancamento();
		lancamento.setId(1);
		lancamento.setDescricao("BURGUER KING");
		lancamento.setValor(20.50);
		lancamento.setData(LocalDate.parse("2019-05-30"));

		Cartao cartao = new Cartao();
		Bandeira bandeira = new Bandeira();
		bandeira.setId(1);
		bandeira.setNome("Mastercard");
		Cliente cliente = new Cliente();
		cliente.setId(1);
		cliente.setNome("Alessandra");
		cliente.setCpf("37137706894");
		cartao.setId(10);
		cartao.setNumero(123456789);
		cartao.setStatus("desativado");
		cartao.setDataFechamento(LocalDate.parse("2019-05-30"));
		cartao.setBandeira(bandeira);
		cartao.setCliente(cliente);

		lancamento.setCartao(cartao);

	}

	@Test
	@WithMockUser
	public void deveSalvarUmLancamento() throws Exception {

		Mockito.when(lancamentoService.cadastrar(any(Lancamento.class))).thenReturn(lancamento);
		String lancamentoJson = mapper.writeValueAsString(lancamento);
		System.out.println("\n\n\n" + lancamentoJson);
		mockMvc.perform(
				post("/auth/lancamento/registrar").content(lancamentoJson).contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk()).andExpect(content().string(lancamentoJson));

	}

	@Test
	@WithMockUser
	public void deveBuscarUmLancamento() throws Exception {

		int id = lancamento.getId();
		Mockito.when(lancamentoService.consultar(lancamento.getId())).thenReturn(lancamento);
		mockMvc.perform(get("/auth/lancamento/consultar/" + id)).andExpect(status().isOk())
				.andExpect(content().string(mapper.writeValueAsString(lancamento)));

	}

	@Test
	@WithMockUser
	public void deveDeletarUmLancamento() throws Exception {
		
		int id = lancamento.getId();
		mockMvc.perform(delete("/auth/lancamento/deletar/" + id)).andExpect(status().is2xxSuccessful());

	}

}
