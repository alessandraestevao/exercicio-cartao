package tech.mastertech.itau.cartao.cartao.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import tech.mastertech.itau.cartao.cartao.models.Cliente;
import tech.mastertech.itau.cartao.cartao.repositories.ClienteRepository;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ClienteServices.class)
public class ClienteServicesTest {
	
	@Autowired
	  private ClienteServices sujeito;
	  
	  @MockBean
	  private ClienteRepository clienteRepository;
	  
	  @Test
	  public void deveCadastrarUmCliente() {
		  
		  String senha = "admin123";
		    
		    Cliente cliente = new Cliente();
		    cliente.setId(1);
		    cliente.setNome("Ale");
		    cliente.setCpf("37137706894");
		    cliente.setNomeUsuario("aleuser");
		    cliente.setSenha(senha);
		    
		    when(clienteRepository.save(cliente)).thenReturn(cliente);
		    
		    Cliente clienteSalvo = sujeito.cadastrar(cliente);
		    
		    verify(clienteRepository).save(cliente);
		    
		    assertEquals(cliente.getId(), clienteSalvo.getId());
		    assertEquals(cliente.getNome(), clienteSalvo.getNome());
		    assertEquals(cliente.getNomeUsuario(), clienteSalvo.getNomeUsuario());
		    assertNotEquals(senha, clienteSalvo.getSenha());

	  
		  
	  
	  }

}
