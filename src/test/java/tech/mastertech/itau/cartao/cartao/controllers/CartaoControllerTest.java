package tech.mastertech.itau.cartao.cartao.controllers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import tech.mastertech.itau.cartao.cartao.models.Bandeira;
import tech.mastertech.itau.cartao.cartao.models.Cartao;
import tech.mastertech.itau.cartao.cartao.models.Cliente;
import tech.mastertech.itau.cartao.cartao.services.CartaoService;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = CartaoController.class)
public class CartaoControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private CartaoService cartaoService;


	private Cartao cartao;
	private Bandeira bandeira;

	private ObjectMapper mapper = new ObjectMapper();

	@Before
	public void preparar() {
		cartao = new Cartao();
		bandeira = new Bandeira();
		bandeira.setId(1);
		bandeira.setNome("Mastercard");
		Cliente cliente = new Cliente();
		cliente.setId(1);
		cliente.setNome("Alessandra");
		cliente.setCpf("37137706894");
		cartao.setId(10);
		cartao.setNumero(123456789);
		System.out.println("teste");
		cartao.setDataFechamento(LocalDate.parse("2019-05-30"));
		cartao.setStatus("desativado");
		cartao.setBandeira(bandeira);
		cartao.setCliente(cliente);

	}

	@Test
	@WithMockUser
	public void deveSalvarUmCartao() throws Exception {

		Mockito.when(cartaoService.cadastrar(any(Cartao.class))).thenReturn(cartao);

		String cartaoJson = mapper.writeValueAsString(cartao);

		System.out.println("\n\n\n" + cartaoJson);

		mockMvc.perform(post("/auth/cartao/cadastrar").content(cartaoJson).contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk()).andExpect(content().string(cartaoJson));

	}
	
	@WithMockUser
	public void deveCadastrarUmaBandeira() throws Exception {
		
		Mockito.when(cartaoService.cadastrarBandeiras(any(Bandeira.class))).thenReturn(bandeira);

		String bandeiraJson = mapper.writeValueAsString(bandeira);

		System.out.println("\n\n\n" + bandeiraJson);

		mockMvc.perform(post("/auth/cartao/cadastrarBandeiras").content(bandeiraJson).contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk()).andExpect(content().string(bandeiraJson));

	}
	

	@Test
	@WithMockUser
	public void deveAlterarStatusDoCartaoParaAtivo() throws Exception {

		String status = "ativado";
		String idCartao = String.valueOf(cartao.getId());
		cartao.setStatus(status);

		when(cartaoService.alterar(cartao.getId(), status)).thenReturn(cartao);

		Map<String, String> payload = new HashMap<>();
		payload.put("status", status);

		String cartaoJson = mapper.writeValueAsString(cartao);
		String payloadJson = mapper.writeValueAsString(payload);

		mockMvc.perform(patch("/auth/cartao/alterar/" + idCartao).content(payloadJson).contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk()).andExpect(content().string(cartaoJson));

	}
	
	
	

}
