package tech.mastertech.itau.cartao.cartao.controllers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import tech.mastertech.itau.cartao.cartao.models.Cliente;
import tech.mastertech.itau.cartao.cartao.services.ClienteServices;


@RunWith(SpringRunner.class)
@WebMvcTest(controllers = ClienteController.class)
public class ClienteControllerTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private ClienteServices clienteService;
	
	private ObjectMapper mapper = new ObjectMapper();
	private Cliente cliente;
    private int id = 1;
    
	@Before
	public void preparar() {
		cliente = new Cliente();
		cliente.setId(1);
		cliente.setCpf("14503459805");	
		cliente.setNome("Elizabete");
	}
	
	@Test
	@WithMockUser
	  public void deveCadastrarUmCliente() throws Exception {
	
		when(clienteService.cadastrar(any(Cliente.class))).thenReturn(cliente);
	    
	    String clienteJson = mapper.writeValueAsString(cliente);
	    
	    mockMvc.perform(
	        post("/cliente/cadastrar")
	        .content(clienteJson)
	        .contentType(MediaType.APPLICATION_JSON_UTF8)
	       )
	      .andExpect(status().isOk())
	      .andExpect(content().string(clienteJson));
	  }
	
	@Test
	@WithMockUser
	  public void deveConsultarUmCliente() throws Exception {
		when(clienteService.getCliente(id)).thenReturn(cliente);
	    
	    mockMvc.perform(get("/auth/cliente/consultar/" + id))
	            .andExpect(status().isOk())
	            .andExpect(content().string(mapper.writeValueAsString(cliente))); 

	  }
//	
//	@Test
//	@WithMockUser
//	  public void deveAlterarNomeDoCliente() throws Exception {
//	    String nome = "Francini";
//	    cliente.setNome(nome);
//	    
//	    when(clienteService.atualizar(id, nome)).thenReturn(cliente);
//	    
//	    Map<String, String> payload = new HashMap<>();
//	    payload.put("valor", nome);
//	    
//	    String clienteJson = mapper.writeValueAsString(cliente);
//	    String payloadJson = mapper.writeValueAsString(payload);
//	    
//	    mockMvc.perform(
//	        patch("/auth/cliente/alterar/" + id)
//	        .content(payloadJson)
//	        .contentType(MediaType.APPLICATION_JSON_UTF8)
//	    )
//	    .andExpect(status().isOk())
//	    .andExpect(content().string(clienteJson));
//	  }

}
