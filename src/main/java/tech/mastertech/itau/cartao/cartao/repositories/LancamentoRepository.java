package tech.mastertech.itau.cartao.cartao.repositories;

import java.time.LocalDate;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import tech.mastertech.itau.cartao.cartao.models.Lancamento;

public interface LancamentoRepository extends CrudRepository<Lancamento, Integer>{
	
	@Query(value= "SELECT * FROM lancamento WHERE cartao_id = ?1 and data >=  ?2  and data <= ?3 ", nativeQuery = true)
	public Iterable<Lancamento> findAllByDataFechamentoAndId(int idCartao, LocalDate dataInicio, LocalDate dataFechamento);

}
