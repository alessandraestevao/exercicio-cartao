
package tech.mastertech.itau.cartao.cartao.controllers;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tech.mastertech.itau.cartao.cartao.models.Bandeira;
import tech.mastertech.itau.cartao.cartao.models.Cartao;
import tech.mastertech.itau.cartao.cartao.services.CartaoService;

@RestController
@RequestMapping("/auth/cartao")
public class CartaoController {

	@Autowired
	private CartaoService cartaoService;

	@PostMapping("/cadastrar")
	public Cartao cadastrar(@RequestBody Cartao cartao) {
		return cartaoService.cadastrar(cartao);
	}

	@PatchMapping("/alterar/{idCartao}")
	public Cartao alterar(@PathVariable int idCartao, @RequestBody Map<String, String> valor) {

		return cartaoService.alterar(idCartao, valor.get("status"));
	
	}

	@PostMapping("/cadastrarBandeiras")
	public Bandeira cadastrarBandeiras(@RequestBody Bandeira bandeira) {
		return cartaoService.cadastrarBandeiras(bandeira);
	}

}
