package tech.mastertech.itau.cartao.cartao.models;

import java.util.List;

public class Fatura {
	
	private int id;
	private Iterable<Lancamento>  listaLancamento;
	private double soma;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public Iterable<Lancamento> getListaLancamento() {
		return listaLancamento;
	}
	public void setListaLancamento(Iterable<Lancamento> listaLancamento) {
		this.listaLancamento = listaLancamento;
	}
	public double getSoma() {
		return soma;
	}
	public void setSoma(double soma) {
		this.soma = soma;
	}
	
	
	
	

}
