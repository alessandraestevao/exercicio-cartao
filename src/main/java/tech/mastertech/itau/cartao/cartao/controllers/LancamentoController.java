package tech.mastertech.itau.cartao.cartao.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import tech.mastertech.itau.cartao.cartao.models.Fatura;
import tech.mastertech.itau.cartao.cartao.models.Lancamento;
import tech.mastertech.itau.cartao.cartao.services.LancamentoService;

@RestController
@RequestMapping("/auth/lancamento")
public class LancamentoController {

	@Autowired
	private LancamentoService lancamentoService;

	@PostMapping("/registrar")
	public Lancamento registrar(@RequestBody Lancamento lancamento) {
		return lancamentoService.cadastrar(lancamento);
	}

	@GetMapping("/consultar/{id}")
	public Lancamento consultar(@PathVariable int id) {
		return lancamentoService.consultar(id);
	}

	@DeleteMapping("/deletar/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public void deletar(@PathVariable int id) {
		lancamentoService.deletar(id);
	}
	
	@GetMapping("/gerarFatura/{idCartao}")
	public Fatura gerarFatura(@PathVariable int idCartao){
		return lancamentoService.gerarFatura(idCartao);
	}

}
