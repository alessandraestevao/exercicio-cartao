package tech.mastertech.itau.cartao.cartao.services;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import tech.mastertech.itau.cartao.cartao.models.Cartao;
import tech.mastertech.itau.cartao.cartao.models.Fatura;
import tech.mastertech.itau.cartao.cartao.models.Lancamento;
import tech.mastertech.itau.cartao.cartao.repositories.LancamentoRepository;

@Service
public class LancamentoService {

	@Autowired
	private LancamentoRepository lancamentoRepository;
	
	@Autowired
	private CartaoService cartaoService;
	

	public Lancamento cadastrar(Lancamento lancamento) {
		Cartao cartao = cartaoService.getCartao(lancamento.getCartao().getId());
		lancamento.setCartao(cartao);
		Lancamento lancamentoRetorno = null;
		if(cartao != null) {
			lancamentoRetorno = lancamentoRepository.save(lancamento);
		}
		return lancamentoRetorno; 
	}

	public Lancamento consultar(int id) {

		Optional<Lancamento> retorno = lancamentoRepository.findById(id);

		if (!retorno.isPresent()) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Lancamento não existe na base de dados");
		}

		return retorno.get();

	}
	
	public void deletar(int id) {
		lancamentoRepository.deleteById(id);
	}
	
	
	public Fatura gerarFatura(int idCartao){
	
		Fatura fatura = new Fatura();
		
		Cartao cartao = cartaoService.getCartao(idCartao);
		LocalDate dataInicio = cartao.getDataFechamento().minus(Period.ofDays(30));		
		Iterable<Lancamento> lancamentos = null;
		if(cartao != null) {
			lancamentos = lancamentoRepository.findAllByDataFechamentoAndId(cartao.getId(), dataInicio, cartao.getDataFechamento());
		} 
		
		fatura.setListaLancamento(lancamentos);
		
		double somaFatura = somaLancamentos(lancamentos);
		fatura.setSoma(somaFatura);
		
		return fatura;  
	
	}
	
	private double somaLancamentos(Iterable<Lancamento> lancamentos) {
		double soma = 0.0;
		
		for (Lancamento lancamento : lancamentos) {
			soma += lancamento.getValor();
			
		}
		
		return soma;
	}

}
