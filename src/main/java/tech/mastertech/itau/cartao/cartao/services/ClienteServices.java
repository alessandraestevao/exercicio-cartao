package tech.mastertech.itau.cartao.cartao.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import tech.mastertech.itau.cartao.cartao.models.Cliente;
import tech.mastertech.itau.cartao.cartao.repositories.ClienteRepository;

@Service
public class ClienteServices {

	@Autowired
	private ClienteRepository clienteRepository;

	public Cliente cadastrar(Cliente cliente) {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

		cliente.setSenha(encoder.encode(cliente.getSenha()));

		return clienteRepository.save(cliente);
	}

	public Cliente atualizar(int id, String valor) {
		Cliente cliente = getCliente(id);
		cliente.setNome(valor);
		return clienteRepository.save(cliente);

	}

	public Cliente getCliente(int id) {
		Optional<Cliente> retorno = clienteRepository.findById(id);

		if (!retorno.isPresent()) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Cliente não existe na base de dados");
		}

		return retorno.get();
	}

	public Cliente fazerLogin(Cliente cliente) {
		Optional<Cliente> clienteOptional = clienteRepository.findByNomeUsuario(cliente.getNomeUsuario());

		if (!clienteOptional.isPresent()) {
			return null;
		}

		Cliente clienteSalvo = clienteOptional.get();

		BCryptPasswordEncoder bPasswordEncoder = new BCryptPasswordEncoder();

		if (bPasswordEncoder.matches(cliente.getSenha(), clienteSalvo.getSenha())) {
			return clienteSalvo;
		}

		return null;
	}

}
