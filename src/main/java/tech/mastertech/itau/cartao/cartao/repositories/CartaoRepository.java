package tech.mastertech.itau.cartao.cartao.repositories;

import org.springframework.data.repository.CrudRepository;

import tech.mastertech.itau.cartao.cartao.models.Cartao;

public interface CartaoRepository extends CrudRepository<Cartao, Integer> {

}
