
package tech.mastertech.itau.cartao.cartao.controllers;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import tech.mastertech.itau.cartao.cartao.models.Cliente;
import tech.mastertech.itau.cartao.cartao.security.JwtTokenProvider;
import tech.mastertech.itau.cartao.cartao.services.ClienteServices;

@RestController
public class LoginController {

	@Autowired
	private ClienteServices clienteService;

	@PostMapping("/login")
	public Map<String, Object> login(@RequestBody Cliente cliente) {
		Cliente clienteSalvo = clienteService.fazerLogin(cliente);

		if (clienteSalvo == null) {
			throw new ResponseStatusException(HttpStatus.FORBIDDEN);
		}

		Map<String, Object> retorno = new HashMap<>();
		retorno.put("cliente", clienteSalvo);

		JwtTokenProvider provider = new JwtTokenProvider();
		String token = provider.criarToken(cliente.getNomeUsuario());
		retorno.put("token", token);

		return retorno;

	}

}
