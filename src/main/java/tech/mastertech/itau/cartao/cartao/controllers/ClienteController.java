
package tech.mastertech.itau.cartao.cartao.controllers;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tech.mastertech.itau.cartao.cartao.models.Cliente;
import tech.mastertech.itau.cartao.cartao.services.ClienteServices;

@RestController
public class ClienteController {

	@Autowired
	private ClienteServices clienteService;
	
	@GetMapping("/auth/cliente/consultar/{id}")
	public Cliente getCliente(@PathVariable int id) {
		return clienteService.getCliente(id);
	}

	@PostMapping("/cliente/cadastrar")
	public Cliente setCliente(@RequestBody Cliente cliente) {
		return clienteService.cadastrar(cliente);

	}

	@PatchMapping("/auth/cliente/alterar/{id}")
	public Cliente atualizaCliente(@PathVariable int id, @RequestBody Map<String, String> valor) {
		return clienteService.atualizar(id, valor.get("nome"));

	}

}
