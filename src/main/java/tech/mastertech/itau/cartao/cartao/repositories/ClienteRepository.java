package tech.mastertech.itau.cartao.cartao.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import tech.mastertech.itau.cartao.cartao.models.Cliente;

public interface ClienteRepository extends CrudRepository<Cliente, Integer>{
	
	public Optional<Cliente> findByNomeUsuario(String nomeUsuario);

}
