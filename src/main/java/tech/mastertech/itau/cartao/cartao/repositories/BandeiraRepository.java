package tech.mastertech.itau.cartao.cartao.repositories;

import org.springframework.data.repository.CrudRepository;

import tech.mastertech.itau.cartao.cartao.models.Bandeira;

public interface BandeiraRepository extends CrudRepository<Bandeira, Integer>{

}
