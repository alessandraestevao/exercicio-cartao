package tech.mastertech.itau.cartao.cartao.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import tech.mastertech.itau.cartao.cartao.models.Bandeira;
import tech.mastertech.itau.cartao.cartao.models.Cartao;
import tech.mastertech.itau.cartao.cartao.models.Cliente;
import tech.mastertech.itau.cartao.cartao.repositories.BandeiraRepository;
import tech.mastertech.itau.cartao.cartao.repositories.CartaoRepository;

@Service
public class CartaoService {

	@Autowired
	private CartaoRepository cartaoRepository;
	@Autowired
	private BandeiraRepository bandeiraRepository;
	@Autowired
	private ClienteServices clienteServices;

	public Cartao cadastrar(Cartao cartao) {
		Cliente cliente = clienteServices.getCliente(cartao.getCliente().getId());
		Bandeira bandeira = getBandeira(cartao.getBandeira().getId());
		cartao.setCliente(cliente);
		cartao.setBandeira(bandeira);
		return cartaoRepository.save(cartao);

	}

	public Cartao alterar(int idCartao, String status) {
		Cartao cartao = getCartao(idCartao);
		cartao.setStatus(status);
		return cartaoRepository.save(cartao);
	}

	public Cartao getCartao(int idCartao) {

		Optional<Cartao> retorno = cartaoRepository.findById(idCartao);
		if (!retorno.isPresent()) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}

		return retorno.get();
	}

	public Bandeira cadastrarBandeiras(Bandeira bandeira) {
		return bandeiraRepository.save(bandeira);
	}

	public Bandeira getBandeira(int idBandeira) {

		Optional<Bandeira> retorno = bandeiraRepository.findById(idBandeira);
		if (!retorno.isPresent()) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}

		return retorno.get();
	}

}
